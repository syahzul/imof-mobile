/**
 * Define our remote server URL here
 */
var loginServerURL        = 'http://mof.demosite.my/demo/login.php';
var allReportServerURL    = 'http://mof.demosite.my/demo/fiscals.php';
var allFieldsServerURL    = 'http://mof.demosite.my/demo/fields.php';
var allTopicsServerURL    = 'http://mof.demosite.my/demo/topics.php';

var loggedIn = false;

jQuery.ajaxSetup ({
    // Disable caching of AJAX responses
    cache: false    // Development: false, Production: true
});

/**
 * Initialize our app
 * Run any function to prepare the app
 */
function init() {
    // bind buttons action
    bindEvents();

    // Load frontpage
    loadPage( 'home.html' );
}


/**
 * Bind event to main buttons
 */
function bindEvents() {

    // Bind action to links with .ajax-link classname
    jQuery('body').on('click', '.ajax-link', function(e) {
        // get data-page attribute
        var page = jQuery(this).attr('data-page');

        // check if we have callback defined in the link
        var callback = jQuery(this).attr('data-callback');

        // call loadPage function and pass the page name
        if (typeof callback === 'undefined') {
            loadPage( page );
        }
        else {
            loadPage(page, window[callback]());
        }

        e.preventDefault();
    });

    // Bind action for login form
    jQuery('body').on('submit', '#login-form', function(e) {
        loginUser();
        e.preventDefault();
    });


    jQuery('body').on('click', '.topic-item', function(e) {

        // get field ID
        var ID = jQuery(this).attr('data-id');
        loadTopicsFromRemoteServer( ID );
        e.preventDefault();

    });
}

/**
 * Populate reports
 */
function loadReportsFromRemoteServer() {
    jQuery.ajax({
        url: allReportServerURL,
        dataType: 'jsonp',
        success: function( result ) {
            if (result.status === 1) {

                var html = '<div class="list-groups">';

                // run the loop
                result.reports.forEach( function(item) {
                    html += '<a href="'+item.file+'" class="list-group-item" data-id="'+item.id+'">';
                    html += item.title;
                    html += '<i class="fa fa-chevron-right pull-right"></i>';
                    html += '</a>';
                });
                html += '</div>';

                // set the content
                jQuery('#page-fiscal .content').html( html );
            }
            else {
                var html = '<div class="alert alert-info">No results</div>';
                // set the content
                jQuery('#page-fiscal .content').html( html );
            }
        }
    });
}


function loadFieldsFromRemoteServer() {
    jQuery.ajax({
        url: allFieldsServerURL,
        dataType: 'jsonp',
        success: function( result ) {

            if (result.status === 1) {

                var html = '<h3>Bidang</h3>';
                html += '<div class="list-groups">';

                // run the loop
                result.items.forEach( function(item) {
                    //html += '<a href="'+item.file+'" class="list-group-item" data-id="'+item.id+'">';
                    html += '<a href="#" class="list-group-item topic-item" data-id="'+item.id+'">';
                    html += item.title;
                    html += '<i class="fa fa-chevron-right pull-right"></i>';
                    html += '</a>';
                });
                html += '</div>';

                // set the content
                jQuery('#page-1pp .content').html( html );
            }
            else {
                var html = '<div class="alert alert-info">No results</div>';
                // set the content
                jQuery('#page-1pp .content').html( html );
            }
        }
    });
}

/**
 * Get topic based on field ID
 *
 * @param id
 */
function loadTopicsFromRemoteServer( id ) {
    jQuery.ajax({
        url: allTopicsServerURL,
        dataType: 'jsonp',
        data: {
            id: id
        },
        success: function( result ) {

            if (result.status === 1) {

                var html = '<h3>Topik</h3>';
                html += '<div class="list-groups">';

                // run the loop
                result.items.forEach( function(item) {
                    //html += '<a href="'+item.file+'" class="list-group-item" data-id="'+item.id+'">';
                    html += '<a href="#" class="list-group-item topic-item" data-id="'+item.id+'">';
                    html += item.title;
                    html += '<i class="fa fa-chevron-right pull-right"></i>';
                    html += '</a>';
                });
                html += '</div>';

                // set the content
                jQuery('#page-1pp .content').html( html );
            }
            else {
                var html = '<div class="alert alert-info">No results</div>';
                // set the content
                jQuery('#page-1pp .content').html( html );
            }
        }
    });
}


/**
 * Load page to index.html file
 *
 * @param name       File name with .html
 * @param callback   Callback function
 */
function loadPage( name, callback ) {
    showPreloader();
    jQuery('#page').load( 'pages/' + name, callback );
    hidePreloader();
}

/**
 * Show preloader
 */
function showPreloader() {
    var html = '<div id="preloader"><div></div></div>';
    jQuery('body').prepend( html );
}

/**
 * Hide preloader
 */
function hidePreloader() {
    // make sure the preloader element exists
    if (jQuery('#preloader').length > 0) {
        // fade it out
        jQuery('#preloader').fadeOut(300, function() {
            // then remove it
            jQuery('#preloader').remove();
        });
    }
}

/**
 * Login user
 */
function loginUser() {
    jQuery.ajax({
        url: loginServerURL,
        data: {
            email: jQuery('#email').val(),
            password: jQuery('#password').val()
        },
        dataType: 'jsonp',
        success: function (result) {

            var html = '';

            console.log(result);

            // remove any message
            jQuery('#login-form .alert').remove();

            if (result.status === 0) {
                // set error message
                html = '<div class="alert alert-danger">' + result.message + '</div>';

                // clear the values
                jQuery('#email').val('');
                jQuery('#password').val('');

                // inject message inside login form, before any input elements
                jQuery('#login-form').prepend(html);
            }
            else {
                html = '<div class="alert alert-success">' + result.message + '</div>';

                // clear the values
                jQuery('#email').val('');
                jQuery('#password').val('');

                // inject message inside login form, before any input elements
                jQuery('#login-form').prepend(html);

                loggedIn = true;

                // run timer to hide login form
                setTimeout(function () {
                    loadPage( 'fiscal.html', 'checkIfLoggedIn' );
                }, 2000);
            }
        }
    });
}

/**
 * Check if the user is logged in
 */
function checkIfLoggedIn() {
    if (loggedIn === false) {
        setTimeout( function() {
            jQuery('#guest-message').slideDown();
        }, 300);
    }
    else {
        setTimeout( function() {
            loadReportsFromRemoteServer();
        }, 300);
    }
}